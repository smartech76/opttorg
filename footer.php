<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package opttorg
 */

?>

	</div><!-- #content -->

	<footer class="container-main-footer">
        <div class="container-footer">
            <div class="container-footer-top hidden-sm hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="footer-navigation">
                            <?php wp_nav_menu('menu=FooterMenu'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-footer-middle">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <!--<h2 class="white">ИвановОптТорг</h2>-->
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                <img src="<?php echo get_template_directory_uri() . '/images/logo.png' ?>" alt="text">
                            </a>
                        </div>
                        <div class="col-sm-4 col-md-4">
                             
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <div class="footer-contacts-control">
                                <!--<div class="footer-contact">
                                    <a href="" class="white-link">+7 961 116-64-22</a>
                                </div>-->
                                <div class="footer-contact">
                                    <a href="" class="white-link">+7 901 997-66-77</a>
                                </div>
                                <div class="footer-contact">
                                    <!— Yandex.Metrika informer —>
                                    <a href="https://metrika.yandex.ru/stat/?id=49416010&amp;from=info..;
target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/49416010/3_1_FFB953FF_FF9..;
style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" class="ym-advanced-informer" data-cid="49416010" data-lang="ru" /></a>
                                    <!— /Yandex.Metrika informer —>

                                    <!— Yandex.Metrika counter —>
                                    <script type="text/javascript" >
                                        (function (d, w, c) {
                                            (w[c] = w[c] || []).push(function() {
                                                try {
                                                    w.yaCounter49416010 = new Ya.Metrika2({
                                                        id:49416010,
                                                        clickmap:true,
                                                        trackLinks:true,
                                                        accurateTrackBounce:true,
                                                        webvisor:true
                                                    });
                                                } catch(e) { }
                                            });

                                            var n = d.getElementsByTagName("script")[0],
                                                s = d.createElement("script"),
                                                f = function () { n.parentNode.insertBefore(s, n); };
                                            s.type = "text/javascript";
                                            s.async = true;
                                            s.src = "https://mc.yandex.ru/metrika/tag.js";;

                                            if (w.opera == "[object Opera]") {
                                                d.addEventListener("DOMContentLoaded", f, false);
                                            } else { f(); }
                                        })(document, window, "yandex_metrika_callbacks2");
                                    </script>
                                    <noscript><div><img src="https://mc.yandex.ru/watch/49416010"; style="position:absolute; left:-9999px;" alt="" /></div></noscript>
                                    <!— /Yandex.Metrika counter —>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
            </div>

            <div class="container-footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="site-info">
                                <a href="<?php echo esc_url( __( 'https://wordpress.org/', 'opttorg' ) ); ?>">
                                    <?php
                                    /* translators: %s: CMS name, i.e. WordPress. */
                                    printf( esc_html__( 'Proudly powered by %s', 'opttorg' ), 'WordPress' );
                                    ?>
                                </a>
                                <span class="sep"> | </span>
                                    <?php
                                    /* translators: 1: Theme name, 2: Theme author. */
                                    printf( esc_html__( 'Theme: %1$s by %2$s.', 'opttorg' ), 'opttorg', '<a href="http://underscores.me/">Underscores.me</a>' );
                                    ?>
                            </div><!-- .site-info -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
