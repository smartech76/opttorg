<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package opttorg
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
    
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'opttorg' ); ?></a>

    <header>
<!--        header mobile start-->
        <div class="container-desktop-header hidden-md hidden-lg">
            <div class="container-fluid">
                <div class="row">
                    <div class="hidden-md hidden-lg col-xs-4 col-sm-3">
                        <div id="header-tablet-menu" class="header-icon pull-left">
                            <div id="header-tablet-menu-icon" class="">
                                <span class="glyphicon glyphicon-th-large"></span>
                            </div>
                            <div id="header-tablet-close-icon" class="hide">
                                <span class="glyphicon glyphicon-remove"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-6 col-md-3">
                        <div class="header-logo">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                <img src="<?php echo get_template_directory_uri() . '/images/logo.png' ?>" alt="text">
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3">
                        <div class="header-icon header-search pull-right">
                            <div id="header-search-icon" class="header-search-icon">
                                <span class="glyphicon glyphicon-th-list"></span>
                            </div>
                            <div id="header-close-icon" class="header-search-icon hide">
                                <span class="glyphicon glyphicon-remove"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!--header tablet menu-->
                <div id="header-menu-widget" class="header-menu-widget-container">
                    <div class="header-menu-widget">
                        <div class="header-tablet-menu-list">
                            <ul>
                                <li><a href="<?php echo get_page_link(16); ?>">Каталог</a></li>
                                <li><a href="<?php echo get_page_link(68); ?>">Опт</a></li>
                                <li><a href="<?php echo get_page_link(63); ?>">Как заказать</a></li>
                                <li><a href="<?php echo get_page_link(70); ?>">СП</a></li>
                                <li><a href="<?php echo get_page_link(66); ?>">Оплата</a></li>
                                <li><a href="<?php echo get_page_link(72); ?>">Доставка</a></li>
                                <li><a href="<?php echo get_page_link(17); ?>">Корзина</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--header search widget-->
                <div id="header-search-widget" class="header-search-widget-container">
                    <div class="header-search-widget">
                        <? if (is_active_sidebar('right-catalog-sidebar')):?>
                        <div id="right-catalog-sidebar" class="s1">
                            <? dynamic_sidebar('right-catalog-sidebar');?>
                        </div>
                        <? endif?>
                    </div>
                </div>
            </div>
        </div>
<!--        header mobile end-->
        <!--        header-desktop-->
        <div class="container-header hidden-xs hidden-sm">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <div class="container-logo">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                <img src="<?php echo get_template_directory_uri() . '/images/logo.png' ?>" alt="text">
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12s">
                                <div class="container-header-control">
                                    <div class="header-control">
                                        <a href="<?php echo get_page_link(85); ?>" class="header-link">Прайс-лист CSV</a>
                                    </div>
                                    <div class="header-control">
                                        <a href="<?php echo get_page_link(88); ?>" class="header-link">Отзывы</a>
                                    </div>
                                    <div class="header-control">
                                        <a href="<?php echo get_page_link(91); ?>" class="header-link">Контакты</a>
                                    </div>
                                    <div class="header-control header-control-search">
                                        <div class="container-header-search">
                                            <?php echo do_shortcode('[wcas-search-form]'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="horizontal-header-separator">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="container-header-menu">
                                    <div class="container-menu-button">
                                        <a href="<?php echo get_page_link(16); ?>">
                                            <button class="menu-button">Каталог</button>
                                        </a>
                                        <a href="<?php echo get_page_link(68); ?>">
                                            <button class="menu-button">Опт</button>
                                        </a>
                                    </div>
                                    <div class="vertical-header-separator">
                                    </div>
                                    <div class="container-menu-button">
                                        <a href="<?php echo get_page_link(63); ?>">
                                            <button class="menu-button">Как заказать</button>
                                        </a>
                                        <a href="<?php echo get_page_link(70); ?>">
                                            <button class="menu-button">СП</button>
                                        </a>
                                    </div>
                                    <div class="vertical-header-separator">
                                    </div>
                                    <div class="container-menu-button">
                                        <a href="<?php echo get_page_link(66); ?>">
                                            <button class="menu-button">Оплата</button>
                                        </a>
                                        <a href="<?php echo get_page_link(72); ?>">
                                            <button class="menu-button">Доставка</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="header-cart-control">
                                    <div class="header-cart-icon">

                                    </div>
                                    <div class="header-cart-text">
                                         <?php wp_nav_menu('menu=HeaderCartMenu'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="header-action-control">
                                    <div class="header-action">
                                        <a href="#" class="eModal-2 recall-me white-link">Перезвоните мне</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="header-contacts-control">
                                    <!--<div class="header-contact">
                                        <a href="" class="phone-block-item phone-block-item-beeline">+7 961 116-64-22</a>
                                    </div>-->
                                    <div class="header-contact">
                                        <a href="" class="phone-block-item phone-block-item-megafon text-big">+7 901  997-66-77</a>
                                    </div>
                                    <!--<div class="header-contact">
                                        <a href="" class="phone-block-item phone-block-item-mts">+7 910 693-78-34</a>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
		<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'opttorg' ); ?></button>
		</nav><!-- #site-navigation -->
        
	</header><!-- #masthead -->
    
    
    

	<div id="content" class="site-content">
