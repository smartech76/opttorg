<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<table class="table text-center vcenter table-hover">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
        <caption class="table-title">Ассортимент <?php woocommerce_page_title(); ?></caption>
	<?php endif; ?>
    <thead>
    <tr class="text-center thead-border">
        <th class="theader-back thead-border">Наименование</th>
        <th class="theader-back thead-border">Размеры</th>
        <th class="theader-back thead-border">Цена</th>
        <th class="theader-back thead-border">Кол-во</th>
        <th class="theader-back thead-border">Корзина</th>
    </tr>
    </thead>
    <tbody>
