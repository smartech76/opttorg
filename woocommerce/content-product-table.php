<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

?>
    <tr class="custom-product-table-row custom-border-bottom">
        <td>
            <?php
            /**
             * woocommerce_before_shop_loop_item hook.
             *
             * @hooked woocommerce_template_loop_product_link_open - 10
             */
            do_action( 'woocommerce_before_shop_loop_item' );
            ?>
            <span class="item-name-a">
            <?php 
            do_action( 'woocommerce_shop_loop_item_title' ); 
            ?>
            </span>
            <?php
            /**
             * woocommerce_after_shop_loop_item hook.
             *
             * @hooked woocommerce_template_loop_product_link_close - 5
             * @hooked woocommerce_template_loop_add_to_cart - 10
             * see function.php
             */
            do_action( 'woocommerce_after_shop_loop_item' );
            ?>
        </td>
        <td>
            <?php
            echo $product->get_short_description();
            ?>
        </td>
        <td>
        <?php
        do_action( "product_table_price" );
        ?>
        </td>
        <td>
            <input type="number" class="product-table-quantity" step="1" min="1" value="1">
        </td>
        <td class="product-table-add-to-cart">
            <?php
            do_action( 'product_table_add_to_cart' );
            ?>
        </td>
    </tr>
