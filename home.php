<?php
/*
Template Name: MainPage
*/

get_header();
?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="container-slider-main">
                <div class="container">
                    <div id="main-slider">
                        <div class="main-slider-item">
                            <img src="<?php echo get_template_directory_uri() . '/images/5.jpg' ?>">
                        </div>
                        <div class="main-slider-item">
                            <img src="<?php echo get_template_directory_uri() . '/images/5.jpg' ?>">
                        </div>
                        <div class="main-slider-item">
                            <img src="<?php echo get_template_directory_uri() . '/images/5.jpg' ?>">
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-advantages">
                <div class="container">
                    <div class="row">

                            <div class="advantages-item col-md-3 col-sm-3 col-lg-3 col-xs-12">
                                <div class="advantages-icon">
                                    <img src="<?php echo get_template_directory_uri() . '/images/utem1.png' ?>">
                                </div>
                                <div class="advantages-text">
                                    <span>Минимальная сумма закупки 3000р</span>
                                </div>
                            </div>
                            <div class="advantages-item col-md-3 col-sm-3 col-lg-3 col-xs-12">
                                <div class="advantages-icon">
                                    <img src="<?php echo get_template_directory_uri() . '/images/utem2.png' ?>">
                                </div>
                                <div class="advantages-text">
                                    <span>Доставка до транспортной компании бесплатна</span>
                                </div>
                            </div>
                            <div class="advantages-item col-md-3 col-sm-3 col-lg-3 col-xs-12">
                                <div class="advantages-icon">
                                    <img src="<?php echo get_template_directory_uri() . '/images/utem3.png' ?>">
                                </div>
                                <div class="advantages-text">
                                    <span>Отгрузка товара в течении 5 рабочих дней</span>
                                </div>
                            </div>
                            <div class="advantages-item col-md-3 col-sm-3 col-lg-3 col-xs-12">
                                <div class="advantages-icon">
                                    <img src="<?php echo get_template_directory_uri() . '/images/utem5.png' ?>">
                                </div>
                                <div class="advantages-text">
                                    <span>Продаем без рядов от 1 штуки</span>
                                </div>
                            </div>

                    </div>
                </div>
            </div>

            <div class="container-about">

                <div class="container-offers-tabs">
                    <div class="container">
                        <!-- Nav tabs -->
                        <ul id="offers-tabs" class="nav nav-tabs offers-tabs-wrapper">
                            <li class="active tab-block-links">
                                <a href="#offers-tab-new" data-toggle="tab"
                                   class="tab-block-link white-link">Новинки</a>
                                <a href="" class="offers-tab-hidden-link white-link">Посмотреть все новинки</a>
                            </li>
                            <li class="tab-block-links">
                                <a href="#offers-tab-hits" data-toggle="tab" class="tab-block-link white-link">Хиты
                                    продаж</a>
                                <a href="" class="offers-tab-hidden-link white-link">Посмотреть все хиты</a>
                            </li>
                            <li class="tab-block-links">
                                <a href="#offers-tab-sale" data-toggle="tab" class="tab-block-link white-link">Распродажа</a>
                                <a href="" class="offers-tab-hidden-link white-link">Вся распродажа</a>
                            </li>
                            <li class="tab-block-links-right">
                                <a href="#" data-toggle="tab" class="tab-block-link white-link">Купить от 1 штуки можно
                                    ТУТ!</a>
                                <a href="" class="offers-tab-hidden-link-right white-link">Посмотреть</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="offers-tab-new">
                                <div class="container">
                                    <h2>Новинки</h2>
                                </div>

                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <?php echo do_shortcode('[product_category per_page="4" orderby="date" order="desc" category="постельное-белье"]'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="offers-tab-hits">
                                <div class="container">
                                    <h2>Хиты продаж</h2>
                                </div>

                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <?php echo do_shortcode('[product_category per_page="4" orderby="date" order="desc" category="постельное-белье"]'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="offers-tab-sale">
                                <div class="container">
                                    <h1>Распродажа</h1>
                                </div>

                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <?php echo do_shortcode('[product_category per_page="4" orderby="date" order="desc" category="постельное-белье"]'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--
                <div class="container-slider-small">
                    <div class="container">
                        <div id="small-slider">
                            <div class="small-slider-item">
                                <img src="img/698.jpg">
                            </div>
                            <div class="small-slider-item">
                                <img src="img/699.jpg">
                            </div>
                            <div class="small-slider-item">
                                <img src="img/709.jpg">
                            </div>
                            <div class="small-slider-item">
                                <img src="img/736.jpg">
                            </div>
                            <div class="small-slider-item">
                                <img src="img/738.jpg">
                            </div>
                            <div class="small-slider-item">
                                <img src="img/762.jpg">
                            </div>
                        </div>
                    </div>
                </div>
                -->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 col-md-3">
                            <div class="block-news">
                                <div class="news">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 news-title-wrapper">
                                            <span class="news-title">Новости</span>
                                            <span class="folder-img">→</span>
                                            <img class="folder-img"
                                                 src="<?php echo get_template_directory_uri() . '/images/icon-folder.png' ?>"
                                                 alt="">
                                            <a href="#" class="news-title-link">архив новостей</a>
                                        </div>
                                    </div>
                                    <?php get_sidebar(); ?>
                                </div>
                            </div>
                            <div class="subscribe-container">
                                <span>Подписаться на новости:</span>
                                <div class="input-block">
                                    <input type="text" title="Введите ваш e-mail" placeholder="введите ваш email">
                                </div>
                                <div class="button-container">
                                    <button type="button" class=" sub-button btn btn-primary btn-lg">
                                        Подписаться
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-9 col-md-9">
                            <?php echo do_shortcode('[product_categories number="" parent="0" columns="3"]'); ?>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 hide-title">
                            <?php
                            while (have_posts()) :
                                the_post();

                                get_template_part('template-parts/content', 'page');

                                // If comments are open or we have at least one comment, load up the comment template.
                                if (comments_open() || get_comments_number()) :
                                    comments_template();
                                endif;

                            endwhile; // End of the loop.
                            ?>
                        </div>
                    </div>
                </div>

            </div>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();