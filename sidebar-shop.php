<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package opttorg
 */

if ( ! is_active_sidebar( 'shop' ) ) {
	return;
}
?>

<aside id="shop-sidebar" class="widget-area">
	<?php dynamic_sidebar( 'shop' ); ?>
</aside><!-- #shop-sidebar -->
