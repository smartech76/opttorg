<?php
/**
 * opttorg functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package opttorg
 */

if (!function_exists('opttorg_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function opttorg_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on opttorg, use a find and replace
         * to change 'opttorg' to the name of your theme in all the template files.
         */
        load_theme_textdomain('opttorg', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'menu-1' => esc_html__('Primary', 'opttorg'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('opttorg_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ));
    }
endif;
add_action('after_setup_theme', 'opttorg_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function opttorg_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('opttorg_content_width', 640);
}

add_action('after_setup_theme', 'opttorg_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function opttorg_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'opttorg'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'opttorg'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'opttorg_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function opttorg_scripts()
{
    wp_enqueue_style('opttorg-style', get_stylesheet_uri());

    wp_enqueue_script('opttorg-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true);

    wp_enqueue_script('opttorg-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'opttorg_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Enabling theme support woocommerce
 */
function woocommerce_support()
{
    add_theme_support('woocommerce');
}

add_action('after_setup_theme', 'woocommerce_support');

/**
 * Connecting your own styles
 */

wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '1.0', null);
wp_register_style('bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.min.css', array(), '1.0', null);
wp_register_style('main', get_template_directory_uri() . '/css/main.css', array(
    "header-cart",
    "style",
    "woocommerce-general",
    "woocommerce-layout"
), '1.0', null);
wp_register_style('style', get_template_directory_uri() . '/css/style.css', array(), '1.0', null);
wp_register_style('news', get_template_directory_uri() . '/css/news.css', array(
    "bootstrap",
    "style-storefront"
), '1.0', null);
wp_register_style('slick', get_template_directory_uri() . '/css/slick.css', array(), '1.0', null);
wp_register_style('footer', get_template_directory_uri() . '/css/footer.css', array(), '1.0', null);
wp_register_style('search', get_template_directory_uri() . '/css/search.css', array(), '1.0', null);
wp_register_style('header-cart', get_template_directory_uri() . '/css/header-cart.css', array(), '1.0', null);
wp_register_style('product', get_template_directory_uri() . '/css/product.css', array(), '1.0', null);
wp_register_style('style-storefront', get_template_directory_uri() . '/css/storefront/style-storefront.css', array("woocommerce-general"), '1.0', null);
wp_register_style('sub-sub-category', get_template_directory_uri() . '/css/sub-sub-category.css', array(), 1.0, null);
wp_register_style('header', get_template_directory_uri() . '/css/header.css', array(
    "main"
), 1.0, null);


if (!is_admin()) {
    wp_enqueue_style('bootstrap');
    wp_enqueue_style('bootstrap-theme');
    wp_enqueue_style('slick');
    wp_enqueue_style('main');
    wp_enqueue_style('footer');
    wp_enqueue_style('search');
    wp_enqueue_style('header-cart');
    wp_enqueue_style('product');
    wp_enqueue_style('style-storefront');
    wp_enqueue_style('style');
    wp_enqueue_style('news');
    wp_enqueue_style('sub-sub-category');
    wp_enqueue_style('header');
}

/**
 * Connecting your own scripts
 */
wp_register_script('bootstrap-js', "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js", array('jquery'), '1.0', true);
wp_register_script('slick', get_template_directory_uri() . '/js/slick/slick.min.js', array('jquery'), '1.0', true);
wp_register_script('sliders', get_template_directory_uri() . '/js/sliders.js', array(
    'jquery',
    'slick'
), '1.0', true);
wp_register_script('widgets', get_template_directory_uri() . '/js/widgets.js', array(
    'jquery',
    "bootstrap-js"
), '1.0', true);


wp_deregister_script('jquery');
wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js', false, false, true);
wp_enqueue_script('jquery');

if (!is_admin()) {
    wp_enqueue_script('bootstrap-js');
    wp_enqueue_script('slick');
    wp_enqueue_script('sliders');
    wp_enqueue_script('widgets');
}

/**
 * Connecting your own sidebar to display the product catalog
 */
function register_my_shop_sidebars()
{

    register_sidebar(
        array(
            'id' => 'shop-sidebar',
            'name' => 'Каталог',
            'description' => 'Перетащите сюда виджеты, чтобы добавить их в сайдбар. Отображается только на страницах магазина.',
            'before_widget' => '<div class="shop-sidebar-widget">',
            'after_widget' => '</div>',
            'before_title' => '<h3 class="shop-sidebar-widget-title">',
            'after_title' => '</h3>'
        )
    );
}

add_action('widgets_init', 'register_my_shop_sidebars');

function catalog_right_sidebar()
{
    register_sidebar(
        array(
            'id' => 'right-catalog-sidebar',
            'name' => 'Каталог правого меню',
            'description' => 'Перетащите сюда виджеты, чтобы добавить их в сайдбар. Отображается только на страницах магазина.',
            'before_widget' => '<div class="">',
            'after_widget' => '</div>',
            'before_title' => '<h2 class="mobile-category">',
            'after_title' => '</h2>',
        )
    );
}

;
add_action('widgets_init', 'catalog_right_sidebar');

/**
 * Register a hook to display the header in the product card in an arbitrary location
 */
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);

add_action('woocommerce_single_product_custom_title', 'woocommerce_template_single_title', 5);
/**
 * Register a hook to hidden the breadcrumb
 */
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

/**
 * Register a hook to hidden the buy button in the table of product
 */
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

/**
 * Register a hook to display the buy button in the table of product
 */
add_action('product_table_add_to_cart', 'woocommerce_template_loop_add_to_cart', 10);

/**
 * Register a hook to display the price in the table of product
 */
add_action('product_table_price', 'woocommerce_template_loop_price', 10);

/**
 * Register a hook to display the custom breadcrumb
 */
add_action('woocommerce_custom_breadcrumb', 'woocommerce_breadcrumb', 5);

/**
 * Adding support for increasing the photo when hovering in the product card
 */
add_theme_support('wc-product-gallery-zoom');
add_theme_support('wc-product-gallery-lightbox');
add_theme_support('wc-product-gallery-slider');

/**
 * Register a hook to display the custom category image
 */
function woocommerce_category_image()
{
    if (is_product_category()) {
        global $wp_query;
        $cat = $wp_query->get_queried_object();

        woocommerce_subcategory_thumbnail($cat);
    }
}

add_action('woocommerce_custom_category_image', 'woocommerce_category_image', 5);

/**
 * Register a hook to display the custom category description
 */
function woocommerce_category_description()
{
    if (is_product_category()) {
        global $wp_query;
        $cat = $wp_query->get_queried_object();
        $description = $cat->description;
        if ($description != "") {
            echo $description;
        }
    }
}

add_action('woocommerce_custom_category_description', 'woocommerce_category_description', 5);


/**
 *Selection of names of parental categories of goods
 */
//function get_name_sub_and_head_category_title() {
//	global $product;
////we receive goods id
//	$product_ids = $product->id;
////we receive category terms on goods id
//	$category = get_the_terms( $product_ids, 'product_cat' );
////we find a category name
//	$category_name = $category[0]->name;
//
////we find a name of parental category
//	$category_parent_id = $category[0]->parent;
////if the level of category is higher 0 that we remove a name of parental
//	if ( $category_parent_id > 0 ) {
//		$category_parent      = get_term( $category_parent_id, 'product_cat', "OBJECT" );
//		$category_parent_name = $category_parent->name;
//	}
//	echo "<h2 class='product_title entry-title'>" . $category_parent_name . " - " . $category_name . "</h2>";
//}
//add_action('woocommerce_single_product_custom_title', 'get_name_sub_and_head_category_title', 4);

