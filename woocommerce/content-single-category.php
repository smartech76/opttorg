<?php
/**
 * A special training template for checking the hooks and templates to different parts of the system.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>


    <div class="row">
        <div class="col-lg-5">
            <div class="shadow-box custom-category-image">
                <?php
                /**
                 * Hook: woocommerce_custom_category_image.
                 *
                 * @hooked woocommerce_category_image - 5
                 * @See function.php
                 */
                do_action( 'woocommerce_custom_category_image' ); ?>
            </div>
        </div>

        <div class="col-lg-7">
            <div class="row">
                <div class="col-lg-12">
                    <span class="about-item color-text-item">Описание</span>
                    <div class="description"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 color-text-item">
                    <p><b>ОБРАТИТЕ ВНИМАНИЕ!</b></p>
                    <p>
                        <?php
                        /**
						* Hook: woocommerce_custom_category_description.
						*
						* @hooked woocommerce_category_description- 5
						* @See function.php
						*/
						do_action( 'woocommerce_custom_category_description' ); 
                        ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
